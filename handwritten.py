#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/2/9 21:58
# @Author  : wangxinxi
# @File    : handwritten.py
# @Software: PyCharm
import numpy as np
from sklearn.datasets import load_digits
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.preprocessing import LabelBinarizer
from NeuralNetWork import  NeuralNetwork
from sklearn.cross_validation import train_test_split

digits = load_digits()
X = digits.data
y = digits.target
X -= X.min()
X /= X.max()

nn = NeuralNetwork([64, 100, 10], 'logistic')
X_train, X_test, y_train, y_test = train_test_split(X, y)
# print(X_train)
# print(y_test)
lable_train = LabelBinarizer().fit_transform(y_train)
lable_test = LabelBinarizer().fit_transform(y_test)
print('start fitting')

nn.fit(X_train, lable_train, epochs=3000)
predictions = []
for i in range(X_test.shape[0]):
    o = nn.predict(X_test[i])
    predictions.append(np.argmax(o))
print(confusion_matrix(y_test, predictions))
print(classification_report(y_test, predictions))