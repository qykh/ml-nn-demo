#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/2/9 21:52
# @Author  : wangxinxi
# @File    : load_digist.py
# @Software: PyCharm
from sklearn.datasets import load_digits
digits = load_digits()
print(digits.data.shape)
import pylab as pl
pl.gray()
pl.matshow(digits.images[0])
pl.show()