#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Created on 2017/1/9
__author__ = '140326'
from NeuralNetWork import NeuralNetwork
import numpy as np

nn = NeuralNetwork([2, 3, 2, 1], 'tanh')
x = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
y = np.array([1, 0, 0, 1])
nn.fit(x, y, 0.1, 10000)
for i in [[0, 0], [0, 1], [1, 0], [1, 1]]:
    print(i, nn.predict(i))
# i = 1
# layers = [2,2,1]
# print(np.random.random((layers[i - 1] + 1, layers[i] + 1)))
# print(np.random.random((layers[i] + 1, layers[i])))
