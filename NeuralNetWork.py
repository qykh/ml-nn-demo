#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/1/8 22:10
# @Author  : wangxinxi
# @File    : NeuralWork.py
# @Software: PyCharm

import numpy as np


def tanh(x):
    return np.tanh(x)


def tanh_derive(x):
    return 1 - tanh(x) * tanh(x)


def logistic(x):
    return 1 / (1 + np.exp(-x))


def logistic_derive(x):
    return logistic(x) * (1 - logistic(x))


# print((2 * np.random.random((10 + 1,  1)) - 1))
# print(logistic_derive(np.array([1,2])))
class NeuralNetwork:
    def __init__(self, layers, activation="tanh"):
        if activation == "tanh":
            self.activation = tanh
            self.activation_derive = tanh_derive
        if activation == "logistic":
            self.activation = logistic
            self.activation_derive = logistic_derive
        self.weights = []
        # 随机生成前一层和后一层的权重
        
        
        # 每一层的权重用二维税随机数表示,范围在: +-0.25
        # for i in range(1, len(layers) - 1):
        #     #因为有偏置项， 所以需要加一
        #     self.weights.append((2 * np.random.random((layers[i - 1] + 1, layers[i] + 1)) - 1) * 0.25)
        #     # 初始化这层的权重，完全是为了网络的最后一层
        #     self.weights.append((2 * np.random.random((layers[i] + 1, layers[i + 1])) - 1) * 0.25)

        ################ 上面的另一种的直观 ################
        nn_deep = len(layers)
        # 从第一层开始 到 倒数第一层
        for i in range(1, nn_deep - 1):
            # 前一层的单元数乘以当前的单元数  因为有偏置项， 所以需要加一
            self.weights.append((2 * np.random.random((layers[i - 1] + 1, layers[i] + 1)) - 1) * 0.25)
            
        # 初始最后一层的权重，完全是为了网络的最后一层最后一层没有偏置， 所以不需要加一
        self.weights.append((2 * np.random.random((layers[nn_deep - 2] + 1, layers[nn_deep - 1])) - 1) * 0.25)
        print(self.weights)
    
    def fit(self, x, y, learning_rate=0.2, epochs=1000):
        # 把特征向量转为2纬
        x = np.atleast_2d(x)
        # 在x输入上的最后加一列，并初始化
        temp = np.ones([x.shape[0], x.shape[1] + 1])
        temp[:, 0:-1] = x
        x = temp
        y = np.array(y)
     
        for k in range(epochs):
            # 随机抽样选取一行
            i = np.random.randint(x.shape[0])
            a = [x[i]]
            
            # 根据抽样正向更新
            for l in range(len(self.weights)):
                # 内积， 和权重相乘
                # 计算下一层的值
                a.append(self.activation(np.dot(a[l], self.weights[l])))
            # 反向计算,更新权重
            error = y[i] - a[-1]
            # 误差项 ，初始化最后一层的
            deltas = [error * self.activation_derive(a[-1])]
            # deltas = [self.activation_derive(a[-1]) * (y[i] - a[-1])]
            # 反向计算误差项
            for l in range(len(a) - 2, 0, -1):
                # 根据后面的误差项计算 前面的误差项
                deltas.append(deltas[-1].dot(self.weights[l].T) * self.activation_derive(a[l]))
            deltas.reverse()
            
            # 梯度下降  更新权重, 遍历每一层的网络
            for j in range(len(self.weights)):
                layer = np.atleast_2d(a[j])
                delta = np.atleast_2d(deltas[j])
                # 学习率乘以节点的数据
                self.weights[j] += learning_rate * layer.T.dot(delta)
    
    def predict(self, x):
        x = np.array(x)
        temp = np.ones(x.shape[0] + 1)
        temp[0:-1] = x
        a = temp
        for l in range(0, len(self.weights)):
            a = self.activation(np.dot(a, self.weights[l]))
        return a
